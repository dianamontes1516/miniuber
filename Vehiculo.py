from random import randrange

class Vehiculo:
    def __init__(self, identificador ,x, y):
        self.identificador =identificador
        self.posicion = [randrange(x), randrange(y)]
        self.destino = None
        self.estado = "libre"
        self.usuario_anterior = None
        self.pasaje = None


    def avanza_en_uno(self):
        #primero avanza en x, si ya no hay espacio, avanza en y
        if self.estado == "asignado" or self.estado == "ocupado":
            if abs(self.destino[0] - self.posicion[0]) > 0:
                if self.destino[0] > self.posicion[0] :
                    self.posicion[0] = self.posicion[0] + 1
                else:
                    self.posicion[0] = self.posicion[0] - 1
            elif abs(self.destino[1] - self.posicion[1]) > 0:
                if self.destino[1] > self.posicion[1] :
                    self.posicion[1] = self.posicion[1] + 1
                else:
                    self.posicion[1] = self.posicion[1] - 1

        if self.estado == "ocupado":
            self.pasaje.posición[0] = self.posicion[0];
            self.pasaje.posición[1] = self.posicion[1];


    def asigna_pasaje(self, usuario):
        self.destino = usuario.posición
        self.estado = "asignado"
        self.pasaje = usuario


    def recoge_pasaje(self):
        print("El usuario ", self.pasaje.identificador, " es recogido.")
        self.estado = "ocupado"
        self.usuario_anterior = self.pasaje.identificador
        self.destino = self.pasaje.destino


    def deja_pasaje(self, uber):
        print("El usuario ", self.pasaje.identificador, " llegó a su destino.")
        uber.libera_usuario_vehículo(self.pasaje.identificador, self.identificador)
        self.estado = "libre"
        self.pasaje = None
        self.destino = None


    def avanza_posicion(self, uber):
        self.avanza_en_uno()
        if self.estado == "asignado":
            if (self.posicion == self.pasaje.posición):
                self.recoge_pasaje()
        if self.estado == "ocupado":
            if (self.posicion == self.pasaje.destino):
                self.deja_pasaje(uber)
