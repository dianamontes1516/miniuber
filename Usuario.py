import random

class Usuario:
    def __init__(self, ciudad, id):
        self.identificador = id
        self.posición = [random.randrange(ciudad[0]),random.randrange(ciudad[1])]
        self.destino = [random.randrange(ciudad[0]),random.randrange(ciudad[1])]
        self.estado = 'esperando'

    def actualiza_posición(self, x, y):
        self.posición = [x,y]

    def genera_destino(self, ciudad):
        self.destino = [random.randrange(ciudad[0]),random.randrange(ciudad[1])]

    def asigan_pasaje(pasaje):
        pass
