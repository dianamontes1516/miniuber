import sys

class Uber:

    def __init__(self, entorno):
        self.entorno = entorno

    def calcula_distancia(pos1, pos2):
        x = abs(pos1[0] - pos2[0])
        y = abs(pos1[1] - pos2[1])
        return x + y

    def envía_vehículo_cercano(self):
        distancia = sys.maxsize
        usuario_libre = self.entorno.usuario_libre()
        vehículos_disponibles = self.entorno.vehículos_disponibles()
        seleccionado = None

        for x in vehículos_disponibles:
            if x.usuario_anterior != usuario_libre.identificador:
                tmp = Uber.calcula_distancia(usuario_libre.posición, x.posicion)
                if tmp < distancia:
                    distancia = tmp
                    seleccionado = x

        if seleccionado is not None:
            seleccionado.asigna_pasaje(usuario_libre)
            self.entorno.ocupa_usuario(usuario_libre.identificador)
            self.entorno.ocupa_vehículo(seleccionado.identificador)
            print("Se asigna el vehículo ", seleccionado.identificador,
                  " al usuario ", usuario_libre.identificador,
                  " con posición", usuario_libre.posición,
                  " y destino", usuario_libre.destino)

    def libera_usuario_vehículo(self, id_usuario, id_vehículo):
        self.entorno.desocupa_vehículo(id_vehículo)
        self.entorno.desocupa_usuario(id_usuario)
