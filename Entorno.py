import random
import time
from Usuario import Usuario
from Vehiculo import Vehiculo
from Uber import Uber

class Entorno:

    def __init__(self, número_usuarios, número_vehículos, ancho_ciudad, largo_ciudad, iteraciones):
        self.ciudad = [ancho_ciudad, largo_ciudad]
        self.usuarios_libres =  [Usuario(self.ciudad,x) for x in range(número_usuarios)]
        self.usuarios_ocupados =  [None] * número_usuarios # ta medio puerco
        self.vehículos_libres = [Vehiculo(x,ancho_ciudad, largo_ciudad) for x in range(número_vehículos)]
        self.vehículos_ocupados = [None] * número_vehículos
        self.iteraciones = iteraciones

    def ocupa_vehículo(self, id_vehículo):
        v = self.vehículos_libres[id_vehículo]
        self.vehículos_libres[id_vehículo] = None
        self.vehículos_ocupados[id_vehículo] = v

    def desocupa_vehículo(self, id_vehículo):
        v = self.vehículos_ocupados[id_vehículo]
        self.vehículos_ocupados[id_vehículo] = None
        self.vehículos_libres[id_vehículo] = v

    def ocupa_usuario(self, id_usuario):
        v = self.usuarios_libres[id_usuario]
        self.usuarios_libres[id_usuario] = None
        self.usuarios_ocupados[id_usuario] = v

    def desocupa_usuario(self, id_usuario):
        v = self.usuarios_ocupados[id_usuario]
        self.usuarios_ocupados[id_usuario] = None
        self.usuarios_libres[id_usuario] = v
        v.genera_destino(self.ciudad) # Medio puerco, tal vez no tendría que estar aquí

    def mueve_vehículos(self,uber):
        for x in self.vehículos_ocupados:
            if x is not None:
                x.avanza_posicion(uber)

    def ejecuta(self):
        uber = Uber(self)
        i = 1
        while i <= self.iteraciones:
            print("\nIteración: ", i)
            self.mueve_vehículos(uber)
            vehículos_l = [[x.identificador, x.posicion] for x in self.vehículos_libres if x is not None]
            vehículos_o = [[x.identificador, x.posicion] for x in self.vehículos_ocupados if x is not None]
            print("vehículos_disponibles",vehículos_l)
            print("vehículos_ocupados",vehículos_o)
            uber.envía_vehículo_cercano()
            i = i + 1


    def usuario_libre(self):
        libres = [x for x in self.usuarios_libres if x is not None]
        return random.choice(libres)

    def vehículos_disponibles(self):
        libres = [x for x in self.vehículos_libres if x is not None]
        return libres


e = Entorno(4,2,6,6,10)
e.ejecuta()
